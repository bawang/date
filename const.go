package date

const (
	DatetimeSecond    = "2006-01-02 15:04:05"
	DatetimeDay       = "2006-01-02"
	DatetimeSecondNum = "20060102150405"
	DatetimeDayNum    = "20060102"
	DateMonth         = "2006-01"
)
