package date

import "time"

func FormatSecond(date time.Time) string{
	return date.Format(DatetimeSecond)
}

func FormatDay(date time.Time)string{
	return date.Format(DatetimeDay)
}

func FormatSecondNum(date time.Time) string{
	return date.Format(DatetimeSecondNum)
}

func FormatDayNum(date time.Time) string{
	return date.Format(DatetimeDayNum)
}
