package date

import (
	"testing"
	"time"
)

func TestBeginOfDay(t *testing.T) {
	date := time.Now().UTC()
	t.Log(BeginOfDay(date))
}

func TestEndOfDay(t *testing.T) {
	date := time.Now().UTC()
	t.Log(FormatSecond(EndOfDay(date)))
}

func TestEndOfMonth(t *testing.T) {
	date := time.Now().AddDate(0, -3, 1).UTC()
	t.Log(FormatSecond(EndOfMonth(date)))
}

func TestBeginOfMonth(t *testing.T) {
	date := time.Now().AddDate(0, -3, 1).UTC()
	t.Log(FormatSecond(BeginOfMonth(date)))
}
