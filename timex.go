package date

import "time"

func BeginOfDay(date time.Time) time.Time {
	duration := time.Duration(0-date.Unix()%(60*60*24)) * time.Second
	newDate := date.Add(duration)
	return newDate
}

func EndOfDay(date time.Time) time.Time {
	duration := time.Duration(60*60*24-date.Unix()%(60*60*24)) * time.Second
	newDate := date.Add(duration).Add(-1 * time.Second)
	return newDate
}

func BeginOfMonth(date time.Time) time.Time {
	newDate := time.Date(date.Year(), date.Month(), 1, 0, 0, 0, 0, time.Local)
	return newDate
}

func EndOfMonth(date time.Time) time.Time {
	date1 := date.AddDate(0, 1, 0)
	newDate := time.Date(date1.Year(), date1.Month(), 1, 23, 59, 59, 0, time.Local)
	return newDate.AddDate(0, 0, -1)
}
